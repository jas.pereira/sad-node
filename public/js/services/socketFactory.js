angular.module('socketService', [])

    // super simple service
    // each function returns a promise object 
    .factory('socketIO', ['$rootScope', function ($rootScope) {
        var socket = io.connect();

        return {
            on: function(eventName, callback) {
                function wrapper() {
                    var args = arguments;
                    $rootScope.$apply(function() {
                        callback.apply(socket, args);
                    });
                }

                socket.on(eventName, wrapper);

                return function() {
                    socket.removeListener(eventName, wrapper);
                };
            }
        };
    }]);