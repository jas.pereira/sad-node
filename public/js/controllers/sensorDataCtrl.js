angular.module('sensorDataCtrl', [])
    .controller('mainController', ['$scope', '$http','socketIO', function($scope, $http, socketIO) {
         $scope.sensors = [];
        
        socketIO.on('allData', function(message){
            var messageJSON = JSON.parse(message);
            $scope.sensors = messageJSON;
            console.log($scope.sensors);
           
        });
        
        socketIO.on('new data', function(message){
             $scope.sensors.push(JSON.parse(message));
             console.log($scope.sensors);
        });
    }]);