// set up ======================================================================
var express = require('express');

var app = express(); 						// create our app w/ express
var http = require('http').Server(app);
var io = require('socket.io')(http);
var nano = require('nano'); 				// nano for cloudant
var database = require('./config/database'); 			// load the database config
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cfenv = require('cfenv');

// configuration ===============================================================

var appEnv = cfenv.getAppEnv(); // get the app environment from Cloud Foundry
app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({'extended': 'true'})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request


// routes ======================================================================
require('./app/routes.js')(app, io);

// listen (start app with node server.js) ======================================
// start server on the specified port and binding host
http.listen(appEnv.port, '0.0.0.0', function() {
	// print a message when the server starts listening
  console.log("server starting on " + appEnv.url);
});