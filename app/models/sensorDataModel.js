module.exports = function(sensor, type, value, group) {
    this.sensor = sensor;
    this.type = type;
    this.value = value;
    this.group = group;
    this.date = getDateTime();


    function getDateTime() {

        var date = new Date();

        var hour = date.getHours();
        hour = (hour < 10 ? "0" : "") + hour;

        var min = date.getMinutes();
        min = (min < 10 ? "0" : "") + min;

        var sec = date.getSeconds();
        sec = (sec < 10 ? "0" : "") + sec;

        var year = date.getFullYear();

        var month = date.getMonth() + 1;
        month = (month < 10 ? "0" : "") + month;

        var day = date.getDate();
        day = (day < 10 ? "0" : "") + day;

        return day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + sec;

    };
};


