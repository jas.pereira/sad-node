
var dbconfig = require('../config/database');
var nano = require('nano')(dbconfig.remoteUrl);
var db = nano.db.use('sad');
var sensorModel = require('./models/sensorDataModel');

module.exports = function(app, io) {

    io.on('connection', (socket) => {
        console.log('a user connected');

        db.view('sensorData', 'sensorData_Index', function(err, body) {
            if (!err) {
                var data = [];
                body.rows.forEach(function(doc) {
                    data.push(doc.value);
                });

                socket.emit('allData', JSON.stringify(data));
                console.log('Socket emit alldata');
            }
        });

        socket.on('disconnect', () => {
            console.log('user disconnected');
        });
    });

    // api ---------------------------------------------------------------------
    // get all todos
    app.get('/api/sensors', function(req, res) {
        db.view('sensorData', 'sensorData_Index', function(err, body) {
            if (!err) {
                var data = [];
                body.rows.forEach(function(doc) {
                    data.push(doc.value);
                });
                res.json(data);
            }
        });
    });


    app.post('/api/sensors', function(req, res) {

        var sensorData = new sensorModel(req.body.sensor, req.body.type,
            req.body.value, req.body.group);

        db.insert(sensorData, function(err, body, header) {
            if (!err) {
                res.send('Successfully added one score to the DB');
                io.sockets.emit('new data', JSON.stringify(sensorData));
            }
            else {
                res.send(err);
            }
        });

    });

    // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });
};