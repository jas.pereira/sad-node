# SAD Cloud
## Version
2016-2017 - 0.0.1

## Setup Cloudant

Create New View with ->
name: sensorData
index name : sensorData_Index

```javascript
function(doc) {
 emit(doc.data, {sensor : doc.sensor, value : doc.value, group : doc.group, date : doc.date, type :doc.type});
}
```